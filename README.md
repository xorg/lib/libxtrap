libXTrap is the Xlib-based client API for the DEC-XTRAP extension.

XTrap was a proposed standard extension for X11R5 which facilitated the
capturing of server protocol and synthesizing core input events.

Digital participated in the X Consortium's xtest working group which chose
to evolve XTrap functionality into the XTEST & RECORD extensions for X11R6.

As X11R6 was released in 1994, XTrap has now been deprecated for over
15 years, and uses of it should be quite rare.

All questions regarding this software should be directed at the
Xorg mailing list:

  https://lists.x.org/mailman/listinfo/xorg

The master development code repository can be found at:

  https://gitlab.freedesktop.org/xorg/lib/libXTrap

Please submit bug reports and requests to merge patches there.

For patch submission instructions, see:

  https://www.x.org/wiki/Development/Documentation/SubmittingPatches

